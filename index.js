const clear = document.getElementById("clear");
const input = document.getElementById("calc__input");
const numberArr = document.querySelectorAll(".items");
const symbols = document.querySelectorAll(".symbol");
let display = true;

numberArr.forEach((number) => {
   number.addEventListener("click", (e) => {
      let target = e.target.textContent
      display = true
      let rootNumber = input.innerHTML;
      let lastChild = rootNumber[rootNumber.length - 1];

      if (target == '*' || target == '/' || target == '-' || target == '+') {
         if (rootNumber != '') {
            if (lastChild == target) {
               display = false
            }else if(lastChild == '*' || lastChild == '/' || lastChild == '-' || lastChild == '+'){
               rootNumber = rootNumber.slice(0, rootNumber.length - 1)
            }
         }else{
            display = false;
         }
      }

      if (display === true) {
         try {
            const numberText = e.target.innerHTML;
            const value = e.target.innerHTML;
            
            input.innerHTML += numberText;
            if (value == "="){
               input.innerHTML = eval(rootNumber);
            }
         } catch (error) {
            input.innerHTML = '';
         }
      }
   })
})

clear.addEventListener("click", () => {
   input.innerHTML = "";
})
